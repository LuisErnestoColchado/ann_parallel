#include <iostream>
#include "ActivationFunction.cpp"

class Neuron
{

	private:

		double MU; //entrada efectiva

		int Layer; //numero de capa

		int TypeFunction; //seleccion de la función de activación

		double ResultActivationFunction;

		double ResultDerivateActivationFunction;

		double Bias;

		bool IsInput;

		double JaccardCoefficient;

		double Amplitude;

	public:

		Neuron();

		Neuron(double,int,int);

		void SetMU(double);

		double GetMU();

		void SetLayer(int);

		int GetLayer();

		void SetTypeFunction(int);

		int GetTypeFunction();

		void SetResultActivationFunction(double);

		double GetResultActivationFunction();

		void SetResultDerivateActivationFunction(double);

		double GetResultDerivateActivationFunction();

		void SetIsInput(bool);

		bool GetIsInput();

		void SetBias(double);

		double GetBias();

		double StartActivateFunction();

		double StartDerivateActivationFunction();

		void UpdateBias(double,double);

		void SetJaccardCoefficient(double);

		double GetJaccardCoefficient();

		void SetAmplitude(double);

		double GetAmplitude();

};

Neuron::Neuron(){};

Neuron::Neuron(double mu,int layer,int typefunction){
	MU = mu;
	Layer = layer;
	TypeFunction = typefunction;
}

void Neuron::SetMU(double mu)
{
	MU = mu;
}

double Neuron::GetMU()
{
	return MU;
}

void Neuron::SetLayer(int layer){
	Layer = layer;
}

int Neuron::GetLayer(){
	return Layer;
}

void Neuron::SetTypeFunction(int typefunction){
	TypeFunction = typefunction;
}

int Neuron::GetTypeFunction(){
	return  TypeFunction;
}

void Neuron::SetIsInput(bool isinput){
	IsInput = isinput;
}

bool Neuron::GetIsInput(){
	return  IsInput;
}

void Neuron::SetBias(double bias){
	Bias = bias;
}

double Neuron::GetBias(){
	return  Bias;
}

void Neuron::SetJaccardCoefficient(double jaccardCoefficient)
{
	JaccardCoefficient = jaccardCoefficient;
}

double Neuron::GetJaccardCoefficient()
{
	return JaccardCoefficient;
}

void Neuron::SetAmplitude(double amplitude)
{
	Amplitude = amplitude;
}

double Neuron::GetAmplitude()
{
	return Amplitude;
}

void Neuron::SetResultActivationFunction(double resultactivationfunction){
	ResultActivationFunction = resultactivationfunction;
}

double Neuron::GetResultActivationFunction(){
	return  ResultActivationFunction;
}

void Neuron::SetResultDerivateActivationFunction(double resultderivateactivationfunction){
	ResultDerivateActivationFunction = resultderivateactivationfunction;
}

double Neuron::GetResultDerivateActivationFunction(){
	return  ResultDerivateActivationFunction;
}

double Neuron::StartActivateFunction(){

	if(TypeFunction == 3 || TypeFunction == 4)
	{
		ActivationFunction activation(MU,TypeFunction);
		activation.SetJaccardCoefficient(JaccardCoefficient);
		activation.SetAmplitude(Amplitude);
		ResultActivationFunction = activation.StartFunction();
	}
	else{
		ActivationFunction activation(MU,TypeFunction);
		ResultActivationFunction = activation.StartFunction();

	}

	return ResultActivationFunction;
}

double Neuron::StartDerivateActivationFunction(){
	ActivationFunction activation(MU,TypeFunction);
	ResultDerivateActivationFunction = activation.DerivateFunction();
	return ResultDerivateActivationFunction;
}

void Neuron::UpdateBias(double learningratio,double error){
	Bias = Bias + (learningratio*error);
}



